<?php

require_once 'helpers.php';

function html_skeleton($page_title, $body) {
    $logout_link = is_signed_in() ? logout_link() : '';
    $account_link = is_signed_in() ? link_to('account.php', "Change password") : '';
    $s = "
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta charset = 'UTF-8'>
        <title>$page_title</title>
    </head>
    <body>
        $logout_link
        $account_link
        $body
    </body>
    </html>
    ";

    return $s;
}

function post_page($posts) {
    $s = post_list($posts);
    if (is_signed_in()) {
        $s .= post_submit_form();
    }

    return $s;
}

function post_list($posts) {
    $s = "<table border='1'>
              <tbody>";

    foreach ($posts as $post) {
        $s .= post_entry($post);
    }

    $s .= "</tbody></table>";

    return $s;
}

function post_submit_form() {
    return "<form id='post-submit' action='home.php' method='post'>
                <label for='content'>Comment</label>
                <textarea name='content'></textarea>
                <input type='submit'>
            </form>";
}

function post_entry($post) {
    $delete_link = "";
    if (current_user()->id === $post->user_id) {
        $delete_link = " <form action='delete.php' method='post'>
                            <input name='post_id' value='$post->id' hidden>
                            <input type='submit' value='(delete)'>
                        </form>";
        }

    return "<tr>
                <td>{$post->user()->username} ($post->timestamp)$delete_link</td>
            </tr>
            <tr>
                <td>$post->content</td>
            </tr>";
}

function login_form($success=true) {
    $s = '';
    if (!$success) {
        $s .= '<p>Username or password incorrect.</p>';
    }

    $s .= "
    <form action = 'login.php' method = 'post'>
        <label for = 'username'>Username</label>
        <input name = 'username'>
        <label for = 'password'>Password</label>
        <input name = 'password' type = 'password'>
        <input type = 'submit' value = 'Log in'>
    </form>
    ";

    return $s;
}

function account_page($success=-1) {
    if ($success === -1) {
        $s = '';
    } else if ($success) {
        $s = "<p>Successfully changed your password</p>";
    } else {
        $s = "<p>Could not change your password.</p>";
    }

    return "<p>$s</p>
            <form action='account.php' method='post'>
                <label for='current_password'>Current password</label>
                <input name='current_password' type='password'>
                <label for='new_password'>New password</label>
                <input name='new_password' type='password'>
                <label for='new_password_confirmation'>Confirm password</label>
                <input name='new_password_confirmation' type='password'>
                <input type='submit' value='Change password'>
            </form>";
}

function registration_form() {
    $s = "
    <form action = 'register.php' method = 'post'>
        <label for = 'username'>Username</label>
        <input name = 'username'>
        <label for = 'email'>Email</label>
        <input name = 'email' type = 'email'>
        <label for = 'password'>Password</label>
        <input name = 'password' type = 'password'>
        <label for = 'password_confirmation'>Password confirmation</label>
        <input name = 'password_confirmation' type = 'password'>
        <label for = 'first_name'>First Name</label>
        <input name = 'first_name'>
        <label for = 'last_name'>Last Name</label>
        <input name = 'last_name'>
        <input type = 'submit' value = 'Register'>
    </form>
    ";
    
    return $s;
}

function logout_link() {
$s = "<form action = 'logout.php' method = 'post' >
        <input type = 'submit' value = 'Logout'>
      </form>";

return $s;
}

function link_to($target, $caption=false) {
    if (!$caption) {
        $caption = $target;
    }

    return "<a href=$target>$caption</a>";
}
