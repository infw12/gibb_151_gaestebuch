<?php

class User {
    public $id;
	public $username;
	public $email;
	public $password_hash;
	public $first_name;
	public $last_name;

	public function __construct($username, $email, $password_hash, $first_name, $last_name) {
		$this->username = $username;
		$this->email = $email;
		$this->password_hash = $password_hash;
		$this->first_name = $first_name;
		$this->last_name = $last_name;
	}
}

