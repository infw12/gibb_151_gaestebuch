<?php

require_once 'templates.php';
require_once 'user_manager.php';

if( $_SERVER['REQUEST_METHOD'] === 'POST') {
    $um = new UserManager();
    $um->read_from_database();
    
    $um->add_from_array($_POST);
} else {
    echo html_skeleton("Register", registration_form());
}

