<?php

require_once 'templates.php';
require_once 'helpers.php';

require_once 'post_manager.php';

redirect_unless_signed_in();

$pm = new PostManager();
$pm->read_from_database();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $user = current_user();
    $post = new Post(current_user()->id,
                    $_POST['content'],
                    date("Y-m-d H:i:s"));

    $pm->add($post);

    redirect_to('home.php');
} else {

    echo html_skeleton("Home", post_page($pm->posts));
}
