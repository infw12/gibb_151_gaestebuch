<?php

require_once 'connection_helper.php';
require_once 'user.php';

require_once 'lib/password.php';

class UserManager {
	public $users;

	public function __construct() {
		$this->users = array();
	}

	public function read_from_database() {
        $this->users = array();

        $mysqli = get_connection();
        $query = "SELECT * FROM user";
        $res = $mysqli->query($query);

        while($object = $res->fetch_object()) {
            $user = new User($object->username, 
                             $object->email, 
                             $object->password_hash,
                             $object->first_name,
                             $object->last_name);
            $user->id = $object->id;

            $this->users[]= $user;
        }
	}

    public function add_from_array($hash) {
        // Todo: Allow empty options.
        if ($hash['password'] === $hash['password_confirmation']) {
                $this->add(new User($hash['username'],
                                         $hash['email'],
                                         password_hash($hash['password'], PASSWORD_BCRYPT),
                                         $hash['first_name'],
                                         $hash['last_name']));
        }
    }

    public function add($user) {
        // Todo: SQL injection um hum. Prepared statements vs mysqli_real_escape_string?
        $mysqli = get_connection();
        $query = "INSERT INTO user (username, email, password_hash, first_name, last_name)
                  VALUES ('$user->username', 
                          '$user->email', 
                          '$user->password_hash', 
                          '$user->first_name', 
                          '$user->last_name')";

        $res = $mysqli->query($query);

       // Todo: Check whether the query was successful. 
        $this->read_from_database();
    }

    public function change_password($user_id, $password) {
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $mysqli = get_connection();
        $query = "UPDATE user SET password_hash = '$hash' WHERE id = $user_id;";
        $res = $mysqli->query($query);
    }


    public function find_by_id($id) {
        foreach ($this->users as $user) {
            if ($user->id === $id) {
                return $user;
            }
        }

        return false;
    }

	public function find_by_username($username) {
		foreach($this->users as $user) {
			if ($user->username === $username)
				return  $user;
		}

		return false;
	}

	public function find_by_email($email) {
		foreach($users as $user) {
			if ($user->email === $email)
				return $user;
		}

		return false;
	}

	public function try_authenticate($username, $password) {
		$user = $this->find_by_username($username);

		if ($user)
			return password_verify($password, $user->password_hash);
		return false;
	}
		
}
