<?php

$db_host = "localhost";
$db_user = "gaestebuch";
$db_password = "geheim";
$db_table = "gibb_151_gaestebuch";

function get_connection() {
    global $db_host, $db_user, $db_password, $db_table;

    return new mysqli($db_host, $db_user, $db_password, $db_table);
}
