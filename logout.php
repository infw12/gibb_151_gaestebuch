<?php

require_once 'helpers.php';

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    session_destroy();
    redirect_to('home.php');
}
