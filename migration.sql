USE gibb_151_gaestebuch;

--DROP TABLE user;

CREATE TABLE IF NOT EXISTS user (id INTEGER NOT NULL AUTO_INCREMENT,
                                 username VARCHAR(100) NOT NULL,
                                 email VARCHAR(100) NOT NULL,
                                 password_hash CHAR(60) NOT NULL,
                                 first_name VARCHAR(100),
                                 last_name VARCHAR(100),
                                 PRIMARY KEY(id));

INSERT INTO user (username, email, password_hash, first_name, last_name) VALUES ('michael', 'michael.senn@swisscom.com', '$2y$10$a4oSlRR7o0ZIRnhA8BiKB.M5iMuBYfQQVQ17kwlh/b5JKk0J9CsL20', 'Michael', 'Senn');


CREATE TABLE IF NOT EXISTS post (id INTEGER NOT NULL AUTO_INCREMENT,
                                 user_id INTEGER NOT NULL,
                                 timestamp DATETIME NOT NULL,
                                 content VARCHAR(500),
                                 PRIMARY KEY (id));
