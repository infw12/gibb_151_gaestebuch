<?php

require_once 'helpers.php';
require_once 'templates.php';
require_once 'user_manager.php';

redirect_unless_signed_in();

if (get_request()) {
    if (was_successful()) {
        $success = 1;
    } else if (was_unsuccessful()) {
        $success = 0;
    } else {
        $success = -1;
    }

    echo html_skeleton("Account settings", account_page($success));
} 
else if (post_request()) {
    $um = new UserManager();
    $um->read_from_database();
    
    if ($_POST['new_password'] === $_POST['new_password_confirmation'] && $um->try_authenticate(current_user()->username, $_POST['current_password'])) {
        $um->change_password(current_user()->id, $_POST['new_password']);
        echo "YAY";
        redirect_to('account.php', array('success' => 1));
    } else {
        echo "NAY";
        redirect_to('account.php', array('success' => 0));
    }
}
