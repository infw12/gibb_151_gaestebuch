<?php

require_once 'templates.php';
require_once 'user_manager.php';

session_start();

function redirect_to($url, $get_args=array()) {
    $query = '';

    if (!empty($get_args)) {
        $query .= '?';
    }
    foreach ($get_args as $key => $value) {
        $query .= "$key=$value";
    }

    header("Location: $url$query");
    $link = link_to($url, $url);
    echo html_skeleton("", "You are being redirected to $link.");
    die();
}

function get_request() {
    return $_SERVER['REQUEST_METHOD'] === "GET";
}

function post_request() {
    return $_SERVER['REQUEST_METHOD'] === "POST";
}

function was_successful() {
    return isset($_GET['success']) && $_GET['success'] === '1';
}

function was_unsuccessful() {
    return isset($_GET['success']) && $_GET['success'] === '0';
}

function current_user() {
    if (!isset($_SESSION['signed_in']) || !isset($_SESSION['username'])) {
        return false;
    }

    $um = new UserManager();
    $um->read_from_database();
    
    return $um->find_by_username($_SESSION['username']);
}

function is_signed_in() {
    return isset($_SESSION['signed_in']) && $_SESSION['signed_in'];
}

function sign_out() {
    unset($_SESSION['username']);
    $_SESSION['signed_in'] = false;
}

function sign_in($username) {
    $_SESSION['signed_in'] = true;
    $_SESSION['username'] = $username;
}

function redirect_if_signed_in($target='home.php') {
    if (is_signed_in())
        redirect_to($target);
}

function redirect_unless_signed_in($target='login.php') {
    if (!is_signed_in()) 
        redirect_to($target);
}
