<?php

require_once 'templates.php';

require_once 'helpers.php';

require_once 'user_manager.php';


@session_start();

redirect_if_signed_in();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $um = new UserManager();
    $um->read_from_database();
    
    if ($um->try_authenticate($_POST['username'], $_POST['password'])) {
        sign_in($_POST['username']);
        redirect_to('home.php');
    } else {
        redirect_to('login.php', array('success' => 0));
    }
} else {
    $success = !(isset($_GET['success']) && $_GET['success'] === '0');
    echo html_skeleton("Login", login_form($success));
}

var_dump($_SESSION);
