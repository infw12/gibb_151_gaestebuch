<?php

require_once 'user_manager.php';

class Post
{
    public $id;
    public $user_id;
    public $content;
    public $timestamp;

    public function __construct($user_id, $content, $timestamp) {
        $this->user_id = $user_id;
        $this->content = $content;
        $this->timestamp = $timestamp;
    }

    public function user() {
        $um = new UserManager();
        $um->read_from_database();

        return $um->find_by_id($this->user_id);
    }

}
