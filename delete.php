<?php
require_once 'helpers.php';
require_once 'post_manager.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    var_dump($_POST);
    redirect_unless_signed_in();
    $pm = new PostManager();
    $pm->read_from_database();

    $post = $pm->find_by_id($_POST['post_id']);
    if ($post->user_id === current_user()->id) {
        $pm->delete($_POST['post_id']);

        redirect_to('home.php');
    }
}

