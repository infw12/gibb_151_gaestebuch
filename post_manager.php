<?php

require_once 'connection_helper.php';
require_once 'post.php';

class PostManager 
{
    public $posts = array();

    public function __construct() {
        $posts = array();
    }

	public function read_from_database() {
        $this->posts = array();

        $mysqli = get_connection();
        $query = "SELECT * FROM post";
        $res = $mysqli->query($query);

        while($object = $res->fetch_object()) {
            $post = new Post($object->user_id, 
                             $object->content, 
                             $object->timestamp);
            $post->id = $object->id;

            $this->posts[]= $post;
        }
    }

    public function add_from_array($array) {
        $this->add(new Post($array['user_id'], $array['content'], $array['timestamp']));
    }
    
    public function add($post) {
        $mysqli = get_connection();
        // Todo: Injection, injection, larilarilarila
        $query = "INSERT INTO post (user_id, timestamp, content) VALUES (
                                                                         '$post->user_id',
                                                                         '$post->timestamp',
                                                                         '$post->content'
                                                                        )";

        $res = $mysqli->query($query);

       // Todo: Check whether the query was successful. 
        $this->read_from_database();
    }

    public function find_by_id($id) {
        foreach ($this->posts as $post) {
            if ($post->id === $id) {
                return $post;
            }
        }
    }

    public function delete($id) {
        $mysqli = get_connection();
        $query = "DELETE FROM post WHERE id = '$id'";
        $res = $mysqli->query($query);

        $this->read_from_database();
    }

    public function find_by_user_id($id) {
        $results = array();

        foreach ($this->posts as $post) {
            if ($post->user_id === $id) {
                $results []= $post;
            }
        }

        return $results;
    }

    public function find_by_user($user) {
        return $this->find_by_user_id($user->id);
    }

}
